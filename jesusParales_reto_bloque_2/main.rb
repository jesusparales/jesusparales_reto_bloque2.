require_relative 'db/connection'
require_relative 'lib/container'
require_relative 'lib/shipping_company'
require_relative 'lib/bl_master'
require_relative 'lib/nvocc'
require_relative 'lib/travel'
require_relative 'lib/vessel'
require_relative 'lib/log_container_master'
require 'active_record'

class Main

  def menu
    system('clear')

    puts "Información de Navieras"
    puts
    puts "1) La representación de los datos de un BL físico"
    puts "2) Un listado todos los BL por naviera"
    puts "3) Listado de la cantidad de contenedores por sus tipos"
    puts "4) Listado del top 10 de países destino con más buques tocando puerto en un periodo de tiempo."
    puts
    print "Ingrese la Opción: "
    var = gets.chomp.to_i

    case var
    when var = 1 then show_fisic_bl
    when var = 2 then bl_x_naviera
    when var = 3 then listado_x_tipo
    when var = 4 then show_top_ten
    else
      puts "Opción Incorrecta"
    end
  end

  def show_fisic_bl
   # b = BlMaster.show_bl
   # b.each do |item|
   #   puts item
   # end
    p "#{BlMaster.show_bl}"
  end

  def bl_x_naviera
    a = BlMaster.list_per_sc
    a.each do |item|
      puts item
    end
  end

  #Busca la cantidad de containar por tipo
  def listado_x_tipo
    p "Cantidad de Listado por Container"
    puts
    p "1) Containers de tipo Standar: #{Container.count_types('ST')}"
    p "2) Containers de tipo High-Cube: #{Container.count_types('HQ')}"
    puts
  end
  def show_top_ten
    p "#{Travel.top_ten}"
  end
end


Main.new.menu
