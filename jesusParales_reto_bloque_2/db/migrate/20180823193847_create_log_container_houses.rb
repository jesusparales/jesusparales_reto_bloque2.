class CreateLogContainerHouses < ActiveRecord::Migration[5.2]
  def change
    create_table :log_container_houses do |t|
      t.references :container, foreign_key: true, null: false
      t.references :bl_house, foreign_key: true, null: false
      t.date :date, null: false
    end
  end
end
