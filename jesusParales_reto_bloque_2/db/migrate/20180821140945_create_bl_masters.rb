class CreateBlMasters < ActiveRecord::Migration[5.2]
  def change
    create_table :bl_masters do |t|
      t.references :shipping_company, foreign_key: true, null: false
      t.references :nvocc, foreign_key: true, null: false
      t.references :travel, foreign_key: true, null: false
      t.date :date, null: false
    end
  end
end
