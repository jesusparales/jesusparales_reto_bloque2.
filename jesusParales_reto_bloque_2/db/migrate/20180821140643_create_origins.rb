class CreateOrigins < ActiveRecord::Migration[5.2]
  def change
    create_table :origins do |t|
      t.references :nvocc, foreign_key: true, null: false
      t.references :consignee, foreign_key: true, null: false
      t.references :country, foreign_key: true, null: false
      t.decimal :price, precision: 5, scale: 2, null: false
    end
  end
end
