class CreateBlHouse < ActiveRecord::Migration[5.2]
  def change
    create_table :bl_houses do |t|
      t.references :bl_master, foreign_key: true, null: false
      t.references :consignee, foreign_key: true, null: false
      t.date :date, null: false
    end
  end
end
