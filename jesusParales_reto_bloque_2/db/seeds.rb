require_relative '../lib/test'
require 'faker'

Dir[File.join('db', 'seeds', '*.rb')].sort.each do |seed|
  puts "Loading seed | #{File.basename(seed)}"
  load seed
end
