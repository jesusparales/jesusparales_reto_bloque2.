
50.times do |index|
  container = Container.all.sample
  blm = BlMaster.all.sample
  LogContainerMaster.create!(container_id: container.id,
                             bl_master_id: blm.id,
                             date: Faker::Time.between(4.month.ago, 1.week.ago))
end
