# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_08_23_200454) do

  create_table "bl_houses", force: :cascade do |t|
    t.integer "bl_master_id", null: false
    t.integer "consignee_id", null: false
    t.date "date", null: false
    t.index ["bl_master_id"], name: "index_bl_houses_on_bl_master_id"
    t.index ["consignee_id"], name: "index_bl_houses_on_consignee_id"
  end

  create_table "bl_masters", force: :cascade do |t|
    t.integer "shipping_company_id", null: false
    t.integer "nvocc_id", null: false
    t.integer "travel_id", null: false
    t.date "date", null: false
    t.index ["nvocc_id"], name: "index_bl_masters_on_nvocc_id"
    t.index ["shipping_company_id"], name: "index_bl_masters_on_shipping_company_id"
    t.index ["travel_id"], name: "index_bl_masters_on_travel_id"
  end

  create_table "consignees", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "containers", force: :cascade do |t|
    t.string "code", null: false
    t.integer "size", null: false
    t.string "kind", null: false
  end

  create_table "countries", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "log_container_houses", force: :cascade do |t|
    t.integer "container_id", null: false
    t.integer "bl_house_id", null: false
    t.date "date", null: false
    t.index ["bl_house_id"], name: "index_log_container_houses_on_bl_house_id"
    t.index ["container_id"], name: "index_log_container_houses_on_container_id"
  end

  create_table "log_container_masters", force: :cascade do |t|
    t.integer "container_id", null: false
    t.integer "bl_master_id", null: false
    t.date "date", null: false
    t.index ["bl_master_id"], name: "index_log_container_masters_on_bl_master_id"
    t.index ["container_id"], name: "index_log_container_masters_on_container_id"
  end

  create_table "nvoccs", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "origins", force: :cascade do |t|
    t.integer "nvocc_id", null: false
    t.integer "consignee_id", null: false
    t.integer "country_id", null: false
    t.decimal "price", precision: 5, scale: 2, null: false
    t.index ["consignee_id"], name: "index_origins_on_consignee_id"
    t.index ["country_id"], name: "index_origins_on_country_id"
    t.index ["nvocc_id"], name: "index_origins_on_nvocc_id"
  end

  create_table "shipping_companies", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "travels", force: :cascade do |t|
    t.integer "vessel_id", null: false
    t.integer "origin", null: false
    t.integer "destination", null: false
    t.date "date", null: false
    t.index ["vessel_id"], name: "index_travels_on_vessel_id"
  end

  create_table "vessels", force: :cascade do |t|
    t.string "name", null: false
    t.integer "shipping_company_id", null: false
    t.index ["shipping_company_id"], name: "index_vessels_on_shipping_company_id"
  end

end
