class Nvocc < ActiveRecord::Base
  has_many :bl_masters, dependent: :destroy
  has_many :origins, dependent: :destroy
  validates :name, presence: { message: "Debes Registrar un Nombre de NVOCC" }

end
