class Origin < ActiveRecord::Base
  belongs_to :country
  belongs_to :nvocc
  belongs_to :consignee
  validates :price, :country_id, :nvocc_id, :consignee_id, presence: true
  validates :price, numericality: { only_integer: true }

end
