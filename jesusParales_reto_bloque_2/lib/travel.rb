class Travel < ActiveRecord::Base
  belongs_to :vessel
  has_one :bl_master
  #has_one :origin, class_name: 'Country', foreign_key: :country_id
  #has_one :destination, class_name: 'Country', foreign_key: :country_id
  validates :date, presence: true

  #----------------------------------#
    #Listado del top 10 de países destino con más buques tocando puerto en un periodo de tiempo

  def self.top_ten
    joins(:vessel).group(:destination).order(id: :desc).take(10)
    #Travel.group(:id).order(id: :desc).take(10)
  end

  private

end
