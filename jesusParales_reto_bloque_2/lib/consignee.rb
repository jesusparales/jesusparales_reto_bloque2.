class Consignee < ActiveRecord::Base
  has_many :bl_houses, dependent: :destroy
  has_many :origins, dependent: :destroy
  validates :name, presence: { message: "Debes Registrar un Nombre de Consignatario" }
  validates :name, uniqueness: { message: "El Nombre ya Existe en la Base de Datos" }
end
