class Vessel < ActiveRecord::Base
  belongs_to :shipping_company
  has_many :travels, dependent: :destroy
  validates :name, presence: { message: "Debes Registrar un Nombre de Buque" }
  validates :name, uniqueness: { message: "El Nombre ya Existe en la Base de Datos" }
  validates :shipping_company_id, presence: { message: "Debes Registrar un ID de la Naviera" }







private

end
