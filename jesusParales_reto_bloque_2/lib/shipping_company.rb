class ShippingCompany < ActiveRecord::Base
  has_many :bl_masters, dependent: :destroy
  has_many :vessels, dependent: :destroy
  validates :name, presence: { message: "Debes Registrar un Nombre de Naviera" }
  validates :name, uniqueness: { message: "El Nombre ya Existe en la Base de Datos" }


  def self.show_list
    joins(:bl_masters).group(:name).count
  end

  def list_of_bl
    "ID: #{id} | Nombre: #{shipping_company.name}"
  end



end
